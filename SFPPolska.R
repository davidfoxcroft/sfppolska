

setwd("~/Documents/Coding/SFPPolska")
rm(list = ls(all = TRUE)) 
pkgs = names(sessionInfo()$otherPkgs)
pkgs = paste('package:', pkgs, sep = "")
lapply(pkgs, detach, character.only = TRUE, unload = TRUE)

library(lme4)

SFPPolska <- read.csv("SFPPolska_2010.11.12.csv", na.strings=c("NA", "NULL"))
SFPPolska$group.0[SFPPolska$group.0 == 2] <- 0
SFPPolska$group.0[SFPPolska$group.0 == 1] <- 1
table(SFPPolska$group.0)

SFPPolska$group.1[SFPPolska$group.1 == 2] <- 0
SFPPolska$group.1[SFPPolska$group.1 == 1] <- 1
table(SFPPolska$group.1)

SFPPolska$group.2[SFPPolska$group.2 == 2] <- 0
SFPPolska$group.2[SFPPolska$group.2 == 1] <- 1
table(SFPPolska$group.2)

table(SFPPolska$mPyt_15.m.0)
table(SFPPolska$tPyt_15.f.0)

famdrinklo <- grep("mPyt_15.m.0", colnames(SFPPolska))
famdrinkhi <- grep("tPyt_15.f.0", colnames(SFPPolska))
SFPPolska$famdrink.0 <- rowMeans(SFPPolska[,c(famdrinklo,famdrinkhi)], na.rm = TRUE)
table(SFPPolska$famdrink.0)

monlo <- grep("Pyt_9a.0", colnames(SFPPolska))
monhi <- grep("Pyt_9i.0", colnames(SFPPolska))
SFPPolska$monitoringmean.0 <- rowMeans(SFPPolska[,c(monlo:monhi)], na.rm = TRUE)

#rescale year of birth for mixed model
year_of_birth.0_s <- scale(SFPPolska$year_of_birth.0)
table(SFPPolska$year_of_birth.0)
table(year_of_birth.0_s)


### drunk alcohol in past 12 months #################################################################################
SFPPolska$Pyt_27a.0[SFPPolska$Pyt_27.0 == 2] <- 0
SFPPolska$Pyt_27a.0[SFPPolska$Pyt_27.0 == 1] <- 1
SFPPolska$Pyt_27a.1[SFPPolska$Pyt_27.1 == 2] <- 0
SFPPolska$Pyt_27a.1[SFPPolska$Pyt_27.1 == 1] <- 1
SFPPolska$Pyt_27a.2[SFPPolska$Pyt_27.2 == 2] <- 0
SFPPolska$Pyt_27a.2[SFPPolska$Pyt_27.2 == 1] <- 1
table(SFPPolska$Pyt_27a.0)
table(SFPPolska$Pyt_27a.1)
table(SFPPolska$Pyt_27a.2)

## Calculate ICCs
library(ICC)
icc.fun <- function(x) {
  df.icc <- data.frame(SFPPolska$Pyt_27a.0,SFPPolska$location.0)
  df.icc[,2] <- as.factor(df.icc[,2])
  colnames(df.icc) <- c("Measure", "Location")  
  ICCest(Location,Measure,df.icc)[1:3]  ###NB check order of variables
}
ICC_out <- icc.fun()
ICC_out

## General Linear Mixed Model (GLMM)
## with random effects (familes nested within communities): 
#fit1form<-as.formula(paste0("Pyt_27a.1 ~ (1|location.0/number.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + Pyt_27a.0"))

## with random effects (communities only): 
fit1form<-as.formula(paste0("Pyt_27a.1 ~ (1|location.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + famdrink.0 + monitoringmean.0 + Pyt_27a.0"))
fit1 <- glmer(fit1form, data = SFPPolska, family = binomial('logit'), na.action=na.omit)
summary(fit1)
fixef(fit1)
exp(fixef(fit1))
ranef(fit1)
library(arm)
display(fit1)
fit1conf <- confint(fit1,method="boot",nsim=100)    #boot",.progress="txt",PBargs=list(style=3))  # takes long time ? parallelize
exp(fit1conf)

fit2form<-as.formula(paste0("Pyt_27a.2 ~ (1|location.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + famdrink.0 + monitoringmean.0 + Pyt_27a.0"))
fit2 <- glmer(fit2form, data = SFPPolska, family = binomial('logit'), na.action=na.omit)
summary(fit2)
fixef(fit2)
exp(fixef(fit2))
ranef(fit2)
library(arm)
display(fit2)
fit2conf <- confint(fit1,method="boot",nsim=100)    #boot",.progress="txt",PBargs=list(style=3))  # takes long time ? parallelize
exp(fit2conf)



## curves of fixed effects:
#install.packages("coefplot2",
#                 repos="http://www.math.mcmaster.ca/bolker/R",
#                 type="source")
library(reshape)
library(coefplot2)
filename<-paste0("Regress_CoeffPlot_GLMM_Fixed_drunkalcohol12_1yr.pdf")
pdf(filename)
coefplot2(fit1)
dev.off()
filename<-paste0("Regress_CoeffPlot_GLMM_Fixed_drunkalcohol12_2yr.pdf")
pdf(filename)
coefplot2(fit2)
dev.off()

## GLMM errors:
filename<-paste0("Regresserrors_GLMM_BinnedPlot_drunkalcohol12_1yr.pdf")
pdf(filename)
binnedplot(fitted(fit1),resid(fit1)) #http://stats.stackexchange.com/questions/63566/unexpected-residuals-plot-of-mixed-linear-model-using-lmer-lme4-package-in-r
dev.off()

filename<-paste0("Regresserrors_GLMM_BinnedPlot_drunkalcohol12_2yr.pdf")
pdf(filename)
binnedplot(fitted(fit2),resid(fit2)) #http://stats.stackexchange.com/questions/63566/unexpected-residuals-plot-of-mixed-linear-model-using-lmer-lme4-package-in-r
dev.off()

## iterate through Community levels, selectively remove and check their influence using Cook's distance
library(influence.ME)
alt.est <- influence(fit1, "location.0")
filename<-paste0("Regresserrors_GLMM_CookDistance_Random_drunkalcohol12_1yr.pdf")
pdf(filename)
print(plot(alt.est, which="cook", sort=TRUE))
dev.off()

alt.est <- influence(fit2, "location.0")
filename<-paste0("Regresserrors_GLMM_CookDistance_Random_drunkalcohol12_2yr.pdf")
pdf(filename)
print(plot(alt.est, which="cook", sort=TRUE))
dev.off()


fit1_est <- fixef(fit1)
group_est <- exp(fit1_est[2])
group_LCI <- exp(fit1conf[3,1])
group_UCI <- exp(fit1conf[3,2])

gender_est <- exp(fit1_est[3])
gender_LCI <- exp(fit1conf[4,1])
gender_UCI <- exp(fit1conf[4,2])

age_est <- exp(fit1_est[4])
age_LCI <- exp(fit1conf[5,1])
age_UCI <- exp(fit1conf[5,2])

SES_est <- exp(fit1_est[5])
SES_LCI <- exp(fit1conf[6,1])
SES_UCI <- exp(fit1conf[6,2])

famdrink_est <- exp(fit1_est[6])
famdrink_LCI <- exp(fit1conf[7,1])
famdrink_UCI <- exp(fit1conf[7,2])

monitoring_est <- exp(fit1_est[7])
monitoring_LCI <- exp(fit1conf[8,1])
monitoring_UCI <- exp(fit1conf[8,2])

baselineuse_est <- exp(fit1_est[8])
baselineuse_LCI <- exp(fit1conf[9,1])
baselineuse_UCI <- exp(fit1conf[9,2])

fit1_ORs <- format(rbind(cbind(ICC_out$ICC,ICC_out$LowerCI,ICC_out$UpperCI),
                         cbind(group_est,group_LCI,group_UCI),
                         cbind(gender_est,gender_LCI,gender_UCI),
                         cbind(age_est,age_LCI,age_UCI),
                         cbind(SES_est,SES_LCI,SES_UCI),
                         cbind(famdrink_est,famdrink_LCI,famdrink_UCI),
                         cbind(monitoring_est,monitoring_LCI,monitoring_UCI),
                         cbind(baselineuse_est,baselineuse_LCI,baselineuse_UCI)),
                   digits=1,nsmall=2)

fit2_est <- fixef(fit2)
group_est <- exp(fit2_est[2])
group_LCI <- exp(fit2conf[3,1])
group_UCI <- exp(fit2conf[3,2])

gender_est <- exp(fit2_est[3])
gender_LCI <- exp(fit2conf[4,1])
gender_UCI <- exp(fit2conf[4,2])

age_est <- exp(fit2_est[4])
age_LCI <- exp(fit2conf[5,1])
age_UCI <- exp(fit2conf[5,2])

SES_est <- exp(fit2_est[5])
SES_LCI <- exp(fit2conf[6,1])
SES_UCI <- exp(fit2conf[6,2])

famdrink_est <- exp(fit2_est[6])
famdrink_LCI <- exp(fit2conf[7,1])
famdrink_UCI <- exp(fit2conf[7,2])

monitoring_est <- exp(fit2_est[7])
monitoring_LCI <- exp(fit2conf[8,1])
monitoring_UCI <- exp(fit2conf[8,2])

baselineuse_est <- exp(fit2_est[8])
baselineuse_LCI <- exp(fit2conf[9,1])
baselineuse_UCI <- exp(fit2conf[9,2])

fit2_ORs <- format(rbind(cbind(ICC_out$ICC,ICC_out$LowerCI,ICC_out$UpperCI),
                         cbind(group_est,group_LCI,group_UCI),
                         cbind(gender_est,gender_LCI,gender_UCI),
                         cbind(age_est,age_LCI,age_UCI),
                         cbind(SES_est,SES_LCI,SES_UCI),
                         cbind(famdrink_est,famdrink_LCI,famdrink_UCI),
                         cbind(monitoring_est,monitoring_LCI,monitoring_UCI),
                         cbind(baselineuse_est,baselineuse_LCI,baselineuse_UCI)),
                   digits=1,nsmall=2)

fit_ORs <- cbind(fit1_ORs,fit2_ORs)

row.names(fit_ORs) <- c("ICC","Experimental Group","Gender","Age","SES","Parental Drinking","Parental Monitoring","Baseline Use")
colnames(fit_ORs) <- c("Estimate.12", "LCI.12", "UCI.12","Estimate.24", "LCI.24", "UCI.24")

library(gridExtra)
#write output
pdf("drunkalcohol12.pdf", height=11, width=8.5)
grid.table(fit_ORs)
dev.off()






### lifetime drunkeness #################################################################################
SFPPolska$Pyt_34a.0[SFPPolska$Pyt_34a.0 == 2] <- 0
SFPPolska$Pyt_34a.0[SFPPolska$Pyt_34a.0 == 1] <- 1
SFPPolska$Pyt_34a.1[SFPPolska$Pyt_34a.1 == 2] <- 0
SFPPolska$Pyt_34a.1[SFPPolska$Pyt_34a.1 == 1] <- 1
SFPPolska$Pyt_34a.2[SFPPolska$Pyt_34a.2 == 2] <- 0
SFPPolska$Pyt_34a.2[SFPPolska$Pyt_34a.2 == 1] <- 1
table(SFPPolska$Pyt_34a.0)
table(SFPPolska$Pyt_34a.1)
table(SFPPolska$Pyt_34a.2)

## Calculate ICCs
library(ICC)
icc.fun <- function(x) {
  df.icc <- data.frame(SFPPolska$Pyt_34a.0,SFPPolska$location.0)
  df.icc[,2] <- as.factor(df.icc[,2])
  colnames(df.icc) <- c("Measure", "Location")  
  ICCest(Location,Measure,df.icc)[1:3]  ###NB check order of variables
}
ICC_out <- icc.fun()
ICC_out



## General Linear Mixed Model (GLMM)
## with random effects (familes nested within communities): 
#fit1form<-as.formula(paste0("Pyt_27a.1 ~ (1|location.0/number.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + Pyt_27a.0"))

## with random effects (communities only): 
fit1form<-as.formula(paste0("Pyt_34a.1 ~ (1|location.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + famdrink.0 + monitoringmean.0 + Pyt_34a.0"))
fit1 <- glmer(fit1form, data = SFPPolska, family = binomial('logit'), na.action=na.omit)
summary(fit1)
fixef(fit1)
exp(fixef(fit1))
ranef(fit1)
library(arm)
display(fit1)
fit1conf <- confint(fit1,method="boot",nsim=100)    #boot",.progress="txt",PBargs=list(style=3))  # takes long time ? parallelize
exp(fit1conf)

fit2form<-as.formula(paste0("Pyt_34a.2 ~ (1|location.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + famdrink.0 + monitoringmean.0 + Pyt_34a.0"))
fit2 <- glmer(fit2form, data = SFPPolska, family = binomial('logit'), na.action=na.omit)
summary(fit2)
fixef(fit2)
exp(fixef(fit2))
ranef(fit2)
library(arm)
display(fit2)
fit2conf <- confint(fit1,method="boot")  #boot",nsim=100)    #boot",.progress="txt",PBargs=list(style=3))  # takes long time ? parallelize
exp(fit2conf)



## curves of fixed effects:
#install.packages("coefplot2",
#                 repos="http://www.math.mcmaster.ca/bolker/R",
#                 type="source")
library(reshape)
library(coefplot2)
filename<-paste0("Regress_CoeffPlot_GLMM_Fixed_lifetimedrunkeness_1yr.pdf")
pdf(filename)
coefplot2(fit1)
dev.off()
filename<-paste0("Regress_CoeffPlot_GLMM_Fixed_lifetimedrunkeness_2yr.pdf")
pdf(filename)
coefplot2(fit2)
dev.off()

## GLMM errors:
filename<-paste0("Regresserrors_GLMM_BinnedPlot_lifetimedrunkeness_1yr.pdf")
pdf(filename)
binnedplot(fitted(fit1),resid(fit1)) #http://stats.stackexchange.com/questions/63566/unexpected-residuals-plot-of-mixed-linear-model-using-lmer-lme4-package-in-r
dev.off()

filename<-paste0("Regresserrors_GLMM_BinnedPlot_lifetimedrunkeness_2yr.pdf")
pdf(filename)
binnedplot(fitted(fit2),resid(fit2)) #http://stats.stackexchange.com/questions/63566/unexpected-residuals-plot-of-mixed-linear-model-using-lmer-lme4-package-in-r
dev.off()

## iterate through Community levels, selectively remove and check their influence using Cook's distance
library(influence.ME)
alt.est <- influence(fit1, "location.0")
filename<-paste0("Regresserrors_GLMM_CookDistance_Random_lifetimedrunkeness_1yr.pdf")
pdf(filename)
print(plot(alt.est, which="cook", sort=TRUE))
dev.off()

alt.est <- influence(fit2, "location.0")
filename<-paste0("Regresserrors_GLMM_CookDistance_Random_lifetimedrunkeness_2yr.pdf")
pdf(filename)
print(plot(alt.est, which="cook", sort=TRUE))
dev.off()

fit1_est <- fixef(fit1)
group_est <- exp(fit1_est[2])
group_LCI <- exp(fit1conf[3,1])
group_UCI <- exp(fit1conf[3,2])

gender_est <- exp(fit1_est[3])
gender_LCI <- exp(fit1conf[4,1])
gender_UCI <- exp(fit1conf[4,2])

age_est <- exp(fit1_est[4])
age_LCI <- exp(fit1conf[5,1])
age_UCI <- exp(fit1conf[5,2])

SES_est <- exp(fit1_est[5])
SES_LCI <- exp(fit1conf[6,1])
SES_UCI <- exp(fit1conf[6,2])

famdrink_est <- exp(fit1_est[6])
famdrink_LCI <- exp(fit1conf[7,1])
famdrink_UCI <- exp(fit1conf[7,2])

monitoring_est <- exp(fit1_est[7])
monitoring_LCI <- exp(fit1conf[8,1])
monitoring_UCI <- exp(fit1conf[8,2])

baselineuse_est <- exp(fit1_est[8])
baselineuse_LCI <- exp(fit1conf[9,1])
baselineuse_UCI <- exp(fit1conf[9,2])

fit1_ORs <- format(rbind(cbind(ICC_out$ICC,ICC_out$LowerCI,ICC_out$UpperCI),
                         cbind(group_est,group_LCI,group_UCI),
                         cbind(gender_est,gender_LCI,gender_UCI),
                         cbind(age_est,age_LCI,age_UCI),
                         cbind(SES_est,SES_LCI,SES_UCI),
                         cbind(famdrink_est,famdrink_LCI,famdrink_UCI),
                         cbind(monitoring_est,monitoring_LCI,monitoring_UCI),
                         cbind(baselineuse_est,baselineuse_LCI,baselineuse_UCI)),
                   digits=1,nsmall=2)

fit2_est <- fixef(fit2)
group_est <- exp(fit2_est[2])
group_LCI <- exp(fit2conf[3,1])
group_UCI <- exp(fit2conf[3,2])

gender_est <- exp(fit2_est[3])
gender_LCI <- exp(fit2conf[4,1])
gender_UCI <- exp(fit2conf[4,2])

age_est <- exp(fit2_est[4])
age_LCI <- exp(fit2conf[5,1])
age_UCI <- exp(fit2conf[5,2])

SES_est <- exp(fit2_est[5])
SES_LCI <- exp(fit2conf[6,1])
SES_UCI <- exp(fit2conf[6,2])

famdrink_est <- exp(fit2_est[6])
famdrink_LCI <- exp(fit2conf[7,1])
famdrink_UCI <- exp(fit2conf[7,2])

monitoring_est <- exp(fit2_est[7])
monitoring_LCI <- exp(fit2conf[8,1])
monitoring_UCI <- exp(fit2conf[8,2])

baselineuse_est <- exp(fit2_est[8])
baselineuse_LCI <- exp(fit2conf[9,1])
baselineuse_UCI <- exp(fit2conf[9,2])

fit2_ORs <- format(rbind(cbind(ICC_out$ICC,ICC_out$LowerCI,ICC_out$UpperCI),
                         cbind(group_est,group_LCI,group_UCI),
                         cbind(gender_est,gender_LCI,gender_UCI),
                         cbind(age_est,age_LCI,age_UCI),
                         cbind(SES_est,SES_LCI,SES_UCI),
                         cbind(famdrink_est,famdrink_LCI,famdrink_UCI),
                         cbind(monitoring_est,monitoring_LCI,monitoring_UCI),
                         cbind(baselineuse_est,baselineuse_LCI,baselineuse_UCI)),
                   digits=1,nsmall=2)

fit_ORs <- cbind(fit1_ORs,fit2_ORs)

row.names(fit_ORs) <- c("ICC","Experimental Group","Gender","Age","SES","Parental Drinking","Parental Monitoring","Baseline Use")
colnames(fit_ORs) <- c("Estimate.12", "LCI.12", "UCI.12","Estimate.24", "LCI.24", "UCI.24")

library(gridExtra)
#write output
pdf("lifetimedrunkeness.pdf", height=11, width=8.5)
grid.table(fit_ORs)
dev.off()






### drunkenesslast12 #################################################################################
SFPPolska$Pyt_34b.0[SFPPolska$Pyt_34b.0 == 2] <- 0
SFPPolska$Pyt_34b.0[SFPPolska$Pyt_34b.0 == 1] <- 1
SFPPolska$Pyt_34b.1[SFPPolska$Pyt_34b.1 == 2] <- 0
SFPPolska$Pyt_34b.1[SFPPolska$Pyt_34b.1 == 1] <- 1
SFPPolska$Pyt_34b.2[SFPPolska$Pyt_34b.2 == 2] <- 0
SFPPolska$Pyt_34b.2[SFPPolska$Pyt_34b.2 == 1] <- 1
table(SFPPolska$Pyt_34b.0)
table(SFPPolska$Pyt_34b.1)
table(SFPPolska$Pyt_34b.2)

## Calculate ICCs
library(ICC)
icc.fun <- function(x) {
  df.icc <- data.frame(SFPPolska$Pyt_34b.0,SFPPolska$location.0)
  df.icc[,2] <- as.factor(df.icc[,2])
  colnames(df.icc) <- c("Measure", "Location")  
  ICCest(Location,Measure,df.icc)[1:3]  ###NB check order of variables
}
ICC_out <- icc.fun()
ICC_out



## General Linear Mixed Model (GLMM)
## with random effects (familes nested within communities): 
#fit1form<-as.formula(paste0("Pyt_27a.1 ~ (1|location.0/number.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + Pyt_27a.0"))

## with random effects (communities only): 
fit1form<-as.formula(paste0("Pyt_34b.1 ~ (1|location.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + famdrink.0 + monitoringmean.0 + Pyt_34b.0"))
#fit1form<-as.formula(paste0("Pyt_34b.1 ~ (1|location.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + famdrink.0 + Pyt_34b.0"))
fit1 <- glmer(fit1form, data = SFPPolska, family = binomial('logit'), na.action=na.omit)
summary(fit1)
fixef(fit1)
exp(fixef(fit1))
ranef(fit1)
library(arm)
display(fit1)
fit1conf <- confint(fit1,method="boot") #nsim=100)    #boot",.progress="txt",PBargs=list(style=3))  # takes long time ? parallelize
exp(fit1conf)

fit2form<-as.formula(paste0("Pyt_34b.2 ~ (1|location.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + famdrink.0 + monitoringmean.0 + Pyt_34b.0"))
#fit2form<-as.formula(paste0("Pyt_34b.2 ~ (1|location.0) + group.0 + gender.0 + year_of_birth.0_s + Pyt_41.0 + famdrink.0 + Pyt_34b.0"))
fit2 <- glmer(fit2form, data = SFPPolska, family = binomial('logit'), na.action=na.omit)
summary(fit2)
fixef(fit2)
exp(fixef(fit2))
ranef(fit2)
library(arm)
display(fit2)
fit2conf <- confint(fit1,method="boot") #,nsim=100)    #boot",.progress="txt",PBargs=list(style=3))  # takes long time ? parallelize
exp(fit2conf)



## curves of fixed effects:
#install.packages("coefplot2",
#                 repos="http://www.math.mcmaster.ca/bolker/R",
#                 type="source")
library(reshape)
library(coefplot2)
filename<-paste0("Regress_CoeffPlot_GLMM_Fixed_drunkenesslast12_1yr.pdf")
pdf(filename)
coefplot2(fit1)
dev.off()
filename<-paste0("Regress_CoeffPlot_GLMM_Fixed_drunkenesslast12_2yr.pdf")
pdf(filename)
coefplot2(fit2)
dev.off()

## GLMM errors:
filename<-paste0("Regresserrors_GLMM_BinnedPlot_drunkenesslast12_1yr.pdf")
pdf(filename)
binnedplot(fitted(fit1),resid(fit1)) #http://stats.stackexchange.com/questions/63566/unexpected-residuals-plot-of-mixed-linear-model-using-lmer-lme4-package-in-r
dev.off()

filename<-paste0("Regresserrors_GLMM_BinnedPlot_drunkenesslast12_2yr.pdf")
pdf(filename)
binnedplot(fitted(fit2),resid(fit2)) #http://stats.stackexchange.com/questions/63566/unexpected-residuals-plot-of-mixed-linear-model-using-lmer-lme4-package-in-r
dev.off()

## iterate through Community levels, selectively remove and check their influence using Cook's distance
library(influence.ME)
alt.est <- influence(fit1, "location.0")
filename<-paste0("Regresserrors_GLMM_CookDistance_Random_drunkenesslast12_1yr.pdf")
pdf(filename)
print(plot(alt.est, which="cook", sort=TRUE))
dev.off()

alt.est <- influence(fit2, "location.0")
filename<-paste0("Regresserrors_GLMM_CookDistance_Random_drunkenesslast12_2yr.pdf")
pdf(filename)
print(plot(alt.est, which="cook", sort=TRUE))
dev.off()

fit1_est <- fixef(fit1)
group_est <- exp(fit1_est[2])
group_LCI <- exp(fit1conf[3,1])
group_UCI <- exp(fit1conf[3,2])

gender_est <- exp(fit1_est[3])
gender_LCI <- exp(fit1conf[4,1])
gender_UCI <- exp(fit1conf[4,2])

age_est <- exp(fit1_est[4])
age_LCI <- exp(fit1conf[5,1])
age_UCI <- exp(fit1conf[5,2])

SES_est <- exp(fit1_est[5])
SES_LCI <- exp(fit1conf[6,1])
SES_UCI <- exp(fit1conf[6,2])

famdrink_est <- exp(fit1_est[6])
famdrink_LCI <- exp(fit1conf[7,1])
famdrink_UCI <- exp(fit1conf[7,2])

monitoring_est <- exp(fit1_est[7])
monitoring_LCI <- exp(fit1conf[8,1])
monitoring_UCI <- exp(fit1conf[8,2])

baselineuse_est <- exp(fit1_est[8])
baselineuse_LCI <- exp(fit1conf[9,1])
baselineuse_UCI <- exp(fit1conf[9,2])

fit1_ORs <- format(rbind(cbind(ICC_out$ICC,ICC_out$LowerCI,ICC_out$UpperCI),
                         cbind(group_est,group_LCI,group_UCI),
                         cbind(gender_est,gender_LCI,gender_UCI),
                         cbind(age_est,age_LCI,age_UCI),
                         cbind(SES_est,SES_LCI,SES_UCI),
                         cbind(famdrink_est,famdrink_LCI,famdrink_UCI),
                         cbind(monitoring_est,monitoring_LCI,monitoring_UCI),
                         cbind(baselineuse_est,baselineuse_LCI,baselineuse_UCI)),
                   digits=3,nsmall=2)

fit2_est <- fixef(fit2)
group_est <- exp(fit2_est[2])
group_LCI <- exp(fit2conf[3,1])
group_UCI <- exp(fit2conf[3,2])

gender_est <- exp(fit2_est[3])
gender_LCI <- exp(fit2conf[4,1])
gender_UCI <- exp(fit2conf[4,2])

age_est <- exp(fit2_est[4])
age_LCI <- exp(fit2conf[5,1])
age_UCI <- exp(fit2conf[5,2])

SES_est <- exp(fit2_est[5])
SES_LCI <- exp(fit2conf[6,1])
SES_UCI <- exp(fit2conf[6,2])

famdrink_est <- exp(fit2_est[6])
famdrink_LCI <- exp(fit2conf[7,1])
famdrink_UCI <- exp(fit2conf[7,2])

monitoring_est <- exp(fit2_est[7])
monitoring_LCI <- exp(fit2conf[8,1])
monitoring_UCI <- exp(fit2conf[8,2])

baselineuse_est <- exp(fit2_est[8])
baselineuse_LCI <- exp(fit2conf[9,1])
baselineuse_UCI <- exp(fit2conf[9,2])

fit2_ORs <- format(rbind(cbind(ICC_out$ICC,ICC_out$LowerCI,ICC_out$UpperCI),
                         cbind(group_est,group_LCI,group_UCI),
                         cbind(gender_est,gender_LCI,gender_UCI),
                         cbind(age_est,age_LCI,age_UCI),
                         cbind(SES_est,SES_LCI,SES_UCI),
                         cbind(famdrink_est,famdrink_LCI,famdrink_UCI),
                         cbind(monitoring_est,monitoring_LCI,monitoring_UCI),
                         cbind(baselineuse_est,baselineuse_LCI,baselineuse_UCI)),
                   digits=3,nsmall=2)

fit_ORs <- cbind(fit1_ORs,fit2_ORs)

row.names(fit_ORs) <- c("ICC","Experimental Group","Gender","Age","SES","Parental Drinking","Parental Monitoring","Baseline Use")
colnames(fit_ORs) <- c("Estimate.12", "LCI.12", "UCI.12","Estimate.24", "LCI.24", "UCI.24")

library(gridExtra)
#write output
pdf("drunkenesslast12.pdf", height=11, width=8.5)
grid.table(fit_ORs)
dev.off()



##########################
data frame for multiple imputation

mi_data <- data.frame(SFPPolska$location.0,
                      SFPPolska$group.0,
                      SFPPolska$gender.0,
                      SFPPolska$year_of_birth.0, 
                      SFPPolska$Pyt_41.0,
                      SFPPolska$famdrink.0,
                      SFPPolska$monitoringmean.0,
                      SFPPolska$Pyt_14a.0,
                      SFPPolska$Pyt_16.0,
                      SFPPolska$Pyt_18.0,
                      SFPPolska$Pyt_25.0,
                      SFPPolska$Pyt_27a.0,
                      SFPPolska$Pyt_27a.1,
                      SFPPolska$Pyt_27a.2,
                      SFPPolska$Pyt_34a.0,
                      SFPPolska$Pyt_34a.1,
                      SFPPolska$Pyt_34a.2,
                      SFPPolska$Pyt_34b.0,
                      SFPPolska$Pyt_34b.1,
                      SFPPolska$Pyt_34b.2)

colnames(mi_data)
require(Amelia)

ncpus = 8
m = ncpus * 1   

imputeddata <- amelia(x = mi_data, m = 1, # number of imputed data sets
                      noms = c("SFPPolska.location.0", "SFPPolska.group.0"),
                      ords = c("SFPPolska.year_of_birth.0", 
                               "SFPPolska.Pyt_41.0",
                               "SFPPolska.Pyt_14a.0",
                               "SFPPolska.Pyt_16.0",
                               "SFPPolska.Pyt_18.0",
                               "SFPPolska.Pyt_25.0",
                               "SFPPolska.Pyt_27a.0",
                               "SFPPolska.Pyt_27a.1",
                               "SFPPolska.Pyt_27a.2",
                               "SFPPolska.Pyt_34a.0",
                               "SFPPolska.Pyt_34a.1",
                               "SFPPolska.Pyt_34a.2",
                               "SFPPolska.Pyt_34b.0",
                               "SFPPolska.Pyt_34b.1",
                               "SFPPolska.Pyt_34b.2"),p2s=2)
                      ,
                                parallel = 'multicore', ncpus = ncpus, p2s=2 )



